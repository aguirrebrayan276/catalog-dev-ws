// path: api/products
import { Router } from 'express';
import { create, getAll, getById, getByName, getByMark, update } from '../controllers/product.controller';

const router = Router();

router.post('/', create);
router.put('/:id', update);
router.get('/', getAll);
router.get('/:id', getById);
router.get('/search/:name', getByName);
router.get('/search/brand/:mark', getByMark);

export default router;