import express from 'express';
import { config } from 'dotenv';
import dbConnection from './database/config';
import ProductRoutes from './routes/product.routes';
 

config();
dbConnection();

const app = express();
app.use(express.json());

app.get('/', (_,response) => {
	response.send({message: "Server Running Successfully"})
});

app.use('/api/products', ProductRoutes); 

app.listen(process.env.PORT || 5000, () => {
	console.log(`Server is running on PORT: ${process.env.PORT || 5000}`);
});
