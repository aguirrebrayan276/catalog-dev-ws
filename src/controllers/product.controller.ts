import { Request, Response } from 'express';
import { networkSuccess, serverError, networkError } from '../middlewares/response.middleware';
import Product from '../models/Product.model';

const getAll = async (_: Request, res: Response) => {
	try {
		const products = await Product.find(); 
		networkSuccess({res,message: 'Lista de productos', data: products})
		return;
	} catch (error) {
		serverError({res, message: 'Ha ocurrido un problema', error});
	}
};

const create = async (req: Request, res: Response) => {
	try { 
		const products = new Product(req.body); 
        //@ts-ignore
        products.discount = products.price_standard - products.price_real;
		await products.save();
		networkSuccess({res,message: 'Producto creado.', status: 201, data: products}) 
		return;
	} catch (error) {
		serverError({res, message: 'Ha ocurrido un problema', error});
	}
};

const getById = async (req: Request, res: Response) => {
	try {
		const { id } = req.params;   
		const product = await Product.findById(id);
		if(product){
			networkSuccess({res,message: `Producto encontrado con ID: ${id}`, data: product}) 
			return;
		}else {
			networkError({res, message: 'Producto no encontrado.', status: 404});
		}
	} catch (error) {
		serverError({res, message: 'Ha ocurrido un problema.', error});
	}
}

const getByName = async (req: Request, res: Response) => {
	try {
		const { name } = req.params;
		const users = await Product.findOne({name}); 
		if(users){
			networkSuccess({res,message: `Producto encontrado con el nombre: ${name}`,data: users}) 
		}else {
			networkError({res, message: 'Producto no encontrado.', status: 404});
		}
	} catch (error) {
		serverError({res, message: 'Ha ocurrido un problema', error});
	}
} 
const getByMark = async (req: Request, res: Response) => {
	try {
		const { mark } = req.params;
		const users = await Product.find({mark}); 
		if(users){
			networkSuccess({res,message: `Producto encontrado en la marca: ${mark}`,data: users}) 
		}else {
			networkError({res, message: 'Producto no encontrado.', status: 404});
		}
	} catch (error) {
		serverError({res, message: 'Ha ocurrido un problema', error});
	}
} 
const update = async (req: Request, res: Response): Promise<void> => {
	try {
		const { id } = req.params;
        const product = await Product.findById(id);
        const newProduct = new Product(req.body);  
        //@ts-ignore
        await Product.updateOne({_id: id},{price_real: newProduct.price_real}); 
        const returnProduct = await Product.findById(id);
		if(product){
			networkSuccess({res,message: 'Producto actualizado.', data: returnProduct}) 
			return;
		}else {
			networkError({res, message: 'Producto no encontrado.', status: 404});
		}
	} catch (error) {
		serverError({res, message: 'Ha ocurrido un problema.', error});
	}
} 

export {
	getAll, create, getById, getByName, getByMark, update
}