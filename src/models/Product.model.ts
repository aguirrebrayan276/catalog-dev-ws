import { Schema, SchemaTypes, model } from 'mongoose';

const ProductSchema = new Schema({
	name: {
		type: String,
		required: true
	},
	image_url: {
		type: String,
		required: true
	},
	description: {
		type: String,
		required: true
	},
	mark: {       
		type: String,
		required: false,
	},
	price_real: {
		type: SchemaTypes.Number,
		required: true 
	}, 
	price_standard: {
		type: SchemaTypes.Number,
		required: true 
	},
    stock: {
		type: SchemaTypes.Number,
		required: true,
		max: 128,
		min: 0
	},
    discount: {
		type: SchemaTypes.Number
	},
    currency: {       
		type: String,
		required: false,
	}, 
	createAT: {
		type: SchemaTypes.Date,
		default: Date.now(),
	}
})

export default model('products', ProductSchema);